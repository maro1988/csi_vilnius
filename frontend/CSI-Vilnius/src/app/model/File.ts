import { logging } from 'protractor'

export class File {
    id: number;
    redNumber: number;
    NNumber: number;
    PNumber: number;
    KNumber: number;
    RNumber: number;
    content: string;
    date: Date;
    secondDate: Date;
    decoded: boolean;

    constructor(data: any) {
        Object.assign(this, data);
    }
}