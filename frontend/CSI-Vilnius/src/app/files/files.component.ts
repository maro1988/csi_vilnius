import { Component, OnInit } from '@angular/core';
import { FileService } from '../service/file.service';


@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {

  // files: Object;
  files: any = [];

  constructor(private fileService: FileService) { }

  // ngOnInit() {
  //   this.fileService.getAllFiles().subscribe(data => {
  //     this.files = data
  //     console.log(this.files);    
  //   });
  // }

  ngOnInit() {
    this.loadFiles();
  }

  loadFiles() {
    return this.fileService.getAllFiles().subscribe((data: {}) => {
      this.files = data;
    })
  }

}
