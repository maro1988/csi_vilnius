package pl.moveschool.CSIVilnius.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.moveschool.CSIVilnius.model.File;
import pl.moveschool.CSIVilnius.repository.FileRepository;

import java.util.List;
import java.util.Optional;

@Service
public class FileService {
    private FileRepository fileRepository;

    @Autowired
    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<File> getAll() {
        return fileRepository.findAll();
    }
    public List<File> saveAll(List<File> files) {
        return fileRepository.saveAll(files);
    }
    public Optional<File> getByRedId(Long rid) {
        return fileRepository.findByRedNumber(rid);
    }
    public Optional<File> getById(Long id) {
        return fileRepository.findById(id);
    }
    public File save(File file) {
        return fileRepository.save(file);
    }
    public ResponseEntity<File> update(File file, Long id) {
        return fileRepository.findById(id).map(fileData -> {
            fileData.setContent(file.getContent());
            fileData.setDate(file.getDate());
            fileData.setSecondDate(file.getSecondDate());
            fileData.setKNumber(file.getKNumber());
            fileData.setNNumber(file.getNNumber());
            fileData.setRNumber(file.getRNumber());
            fileData.setPNumber(file.getPNumber());
            fileData.setRedNumber(file.getRedNumber());
            fileData.setDecoded(file.isDecoded());
            return ResponseEntity.ok().body(fileRepository.save(fileData));
        }).orElse(ResponseEntity.notFound().build());
    }
    public ResponseEntity<?> delete(Long id) {
        return fileRepository.findById(id).map(file ->{
            fileRepository.delete(file);
            return ResponseEntity.ok().build();
        }).orElse(ResponseEntity.notFound().build());
    }
}
