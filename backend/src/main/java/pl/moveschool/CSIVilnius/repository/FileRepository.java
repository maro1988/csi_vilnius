package pl.moveschool.CSIVilnius.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.moveschool.CSIVilnius.model.File;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {
    Optional<File> findByRedNumber(Long rid);
}
