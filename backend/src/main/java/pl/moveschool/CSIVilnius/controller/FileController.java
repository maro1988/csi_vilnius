package pl.moveschool.CSIVilnius.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.moveschool.CSIVilnius.model.File;
import pl.moveschool.CSIVilnius.service.FileService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200")
public class FileController {
    private FileService fileService;

    @Autowired
    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/files")
    public List<File> get() {
        return fileService.getAll();
    }

    @GetMapping("/files/rid{rid}")
    public ResponseEntity<File> getByRId(@PathVariable Long rid){
        return fileService.getByRedId(rid).map(file -> ResponseEntity.ok(file)).orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/files/{id}")
    public ResponseEntity<File> getById(@PathVariable Long id){
        return fileService.getById(id).map(file -> ResponseEntity.ok(file)).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/files")
    public File save(@Valid @RequestBody File file) {
        return fileService.save(file);
    }

    @PutMapping("/files/{id}")
    public ResponseEntity<File> update(@Valid @RequestBody File file, @PathVariable Long id) {
        return fileService.update(file, id);
    }

    @DeleteMapping("/files/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return fileService.delete(id);
    }

    @GetMapping("/security")
    public String testSecurity(){
        return "Security test passed!";
    }
}
