package pl.moveschool.CSIVilnius;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import pl.moveschool.CSIVilnius.model.File;
import pl.moveschool.CSIVilnius.service.FileService;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@SpringBootApplication
public class CsiVilniusApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsiVilniusApplication.class, args);
	}

//	@Bean
//	CommandLineRunner runner(FileService fileService){
//		return args -> {
//			ObjectMapper mapper = new ObjectMapper();
//			TypeReference<List<File>> typeReference = new TypeReference<List<File>>() {};
//			InputStream inputStream = TypeReference.class.getResourceAsStream("/json/files.json");
//			try{
//				List<File> files = mapper.readValue(inputStream,typeReference);
//				fileService.saveAll(files);
//				System.out.println("Files Saved!");
//			} catch (IOException e){
//				System.out.println("Unable to save files: " + e.getMessage());
//			}
//		};
//	}

}
