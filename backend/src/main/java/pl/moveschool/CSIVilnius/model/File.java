package pl.moveschool.CSIVilnius.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "files")
public class File {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long redNumber;
    private Long NNumber;
    private Long PNumber;
    private Long KNumber;
    private Long RNumber;
    private String content;
    private Date date;
    private Date secondDate;
    private boolean decoded;

    public File() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRedNumber() {
        return redNumber;
    }

    public void setRedNumber(Long redNumber) {
        this.redNumber = redNumber;
    }

    public Long getNNumber() {
        return NNumber;
    }

    public void setNNumber(Long NNumber) {
        this.NNumber = NNumber;
    }

    public Long getPNumber() {
        return PNumber;
    }

    public void setPNumber(Long PNumber) {
        this.PNumber = PNumber;
    }

    public Long getKNumber() {
        return KNumber;
    }

    public void setKNumber(Long KNumber) {
        this.KNumber = KNumber;
    }

    public Long getRNumber() {
        return RNumber;
    }

    public void setRNumber(Long RNumber) {
        this.RNumber = RNumber;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getSecondDate() {
        return secondDate;
    }

    public void setSecondDate(Date secondDate) {
        this.secondDate = secondDate;
    }

    public boolean isDecoded() {
        return decoded;
    }

    public void setDecoded(boolean decoded) {
        this.decoded = decoded;
    }

    @Override
    public String toString() {
        return "File{" +
                "id=" + id +
                ", redNumber=" + redNumber +
                ", NNumber=" + NNumber +
                ", PNumber=" + PNumber +
                ", KNumber=" + KNumber +
                ", RNumber=" + RNumber +
                ", content='" + content + '\'' +
                ", date=" + date +
                ", secondDate=" + secondDate +
                ", isDecoded=" + decoded +
                '}';
    }
}
